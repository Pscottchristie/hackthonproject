package com.example.conygre.spring.service;

import java.util.Collection;
import java.util.Optional;

import com.example.conygre.spring.entities.Trade;
import com.example.conygre.spring.entities.TradeAction;
import com.example.conygre.spring.entities.TradeStatus;

import org.bson.types.ObjectId;

public interface TradeService {

    // to be implemented
    Collection<Trade> getTrades();
    Trade addTrade(String ticker, double quantity, double price, TradeAction action);
    Optional<Trade> getById(ObjectId id);
    Trade updateTrade(ObjectId id, TradeStatus tradeStatus);
    Collection<Trade> getAllbyTicker(String tickerName);
    Collection<Trade> getAllbyDate(int date);
    void deleteTrade(ObjectId tradeId);
    void deleteTrades();
    Collection<Trade> getAllbyTickerAndDate(String ticker, int dateRange);

    
}