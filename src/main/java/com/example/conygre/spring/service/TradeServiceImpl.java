package com.example.conygre.spring.service;

import java.util.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

import javax.management.Query;

import com.example.conygre.spring.entities.Trade;
import com.example.conygre.spring.entities.TradeAction;
import com.example.conygre.spring.entities.TradeStatus;
import com.example.conygre.spring.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.query.Criteria;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository repo;

    @Override
    public Collection<Trade> getTrades() {
        return repo.findAll();
    }

    @Override
    public Trade addTrade(String ticker, double quantity, double price, TradeAction action) {
        
        Trade trade = new Trade(new ObjectId(), LocalDateTime.now(), ticker, quantity, price, TradeStatus.CREATED, action);
        repo.insert(trade);

        return trade;
    }

    @Override
    public Optional<Trade> getById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public Trade updateTrade(ObjectId id, TradeStatus tradeStatus) {
        Trade trade = repo.findById(id).get();
        if(trade == null){
            System.out.println("This trade does not exist");
        }
        if (trade.getStatus() == TradeStatus.CREATED) {
            trade.setStatus(tradeStatus);
            repo.save(trade);
            return trade;
        }
        return trade;
    }

    @Override
    public Collection<Trade> getAllbyTickerAndDate(String ticker, int dateRange) {

        LocalDateTime currentDate = LocalDateTime.now();
        LocalDateTime startDate = currentDate.minusDays(dateRange);


        Collection<Trade> dateMatches = repo.findByDateBetween(startDate, currentDate);
        Collection<Trade> tickerMatches = repo.findByTicker(ticker);

        dateMatches.retainAll(tickerMatches);

        System.out.println(dateMatches);
        System.out.println(tickerMatches);

        return dateMatches;
    }


    @Override
    public Collection<Trade> getAllbyTicker(String tickerName) {
        
        return repo.findByTicker(tickerName);
    
    }

    @Override
    public Collection<Trade> getAllbyDate(int date) {
        return repo.findAllByDate(date);
    }

    @Override
    public void deleteTrade(ObjectId tradeId) {
        repo.deleteById(tradeId);
    }

    @Override
    public void deleteTrades() {
        repo.deleteAll();
    }



}