package com.example.conygre.spring.sim;

import java.util.List;

import com.example.conygre.spring.controller.TradeController;
import com.example.conygre.spring.entities.Trade;
import com.example.conygre.spring.entities.TradeStatus;
import com.example.conygre.spring.repo.TradeRepository;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.Logger;


@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeRepository repo;

    @Autowired
    private TradeController controller;

    @Transactional
    public List<Trade> findTradesForProcessing(){
        List<Trade> foundTrades = repo.findByStatus(TradeStatus.CREATED);

        for(Trade trade: foundTrades) {
            trade.setStatus(TradeStatus.PENDING);
            controller.updateTrade(trade);
            repo.save(trade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling(){
        List<Trade> foundTrades = repo.findByStatus(TradeStatus.PENDING);

        for(Trade trade: foundTrades) {
            if((int) (Math.random()*10) > 1) {
                trade.setStatus(TradeStatus.FILLED);
                controller.updateTrade(trade);
            }
            else {
                trade.setStatus(TradeStatus.REJECTED);
                controller.updateTrade(trade);

            }
            repo.save(trade);
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:30000}")
    public void runSim() {
        LOG.debug("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.debug("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.debug("Found " + tradesForProcessing + " trades to be processed");

    }
}
