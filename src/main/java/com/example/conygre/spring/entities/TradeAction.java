package com.example.conygre.spring.entities;

public enum TradeAction {
    BUY, 
    SELL
}
