package com.example.conygre.spring.entities;

public enum TradeStatus {
    CREATED,
    PENDING,
    CANCELLED,
    REJECTED,
    FILLED,
    PARTIALLYFILLED,
    ERROR
}
