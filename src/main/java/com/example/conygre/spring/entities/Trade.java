package com.example.conygre.spring.entities;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
    
    @Id
    private ObjectId id;
    private LocalDateTime date;
    private String ticker;
    private double stockQuantity;
    private double requestedPrice;
    private TradeStatus status;
    private TradeAction action;

    // Constructor for a trade object
    public Trade(ObjectId id, LocalDateTime date, String ticker, double stockQuantity, double requestedPrice, TradeStatus status, TradeAction action) {
        this.id = id;
        this.date = date;
        this.ticker = ticker;
        this.stockQuantity = stockQuantity;
        this.requestedPrice = requestedPrice;
        this.status = status;
        this.action = action;
    }

    // Default constructor for a trade object, shouldn't be used
    public Trade() {
    }

    public ObjectId getId() {
        return id;
    }

    public TradeAction getAction() {
        return action;
    }

    public LocalDateTime getdate() {
        return date;
    }

    public void setdate(LocalDateTime date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public TradeStatus getStatus() {
        return status;
    }

    public void setStatus(TradeStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        Trade c = (Trade) o;

        return id.equals(c.id);
    }

}