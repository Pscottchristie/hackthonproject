package com.example.conygre.spring.repo;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.example.conygre.spring.entities.Trade;
import com.example.conygre.spring.entities.TradeStatus;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    
    Collection<Trade> findByTicker(String ticker);
    Collection<Trade> findAllByDate(int date);
    Optional<Trade> findById(ObjectId id);
    Collection<Trade> findByDateBetween(LocalDateTime createdTo, LocalDateTime createdEnd);
    List<Trade> findByStatus(TradeStatus status);


}