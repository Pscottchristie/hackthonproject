package com.example.conygre.spring.controller;

import java.util.Collection;
import java.util.Optional;

import com.example.conygre.spring.entities.Trade;
import com.example.conygre.spring.entities.TradeAction;
import com.example.conygre.spring.entities.TradeStatus;
import com.example.conygre.spring.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/trades")
public class TradeController {

    @Autowired
    private TradeService service;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Trade> getTrades(){
        return service.getTrades();
    }

    @RequestMapping(method=RequestMethod.GET, value = "/{ticker}/{date}")
    public Collection<Trade> getTradesByTickerDate(@PathVariable final String ticker, @PathVariable final Integer date) {
        return service.getAllbyTickerAndDate(ticker, date);
    }
    
    // add trade to the trade databse for validation
    // send to the portfolio databse in the unvalidated/pending state
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method={RequestMethod.POST,RequestMethod.GET}, value = "{action}/{ticker}/{quantity}/{price}/{portfolioId}")
    public Trade addTrade(@PathVariable final String ticker, @PathVariable final double quantity, @PathVariable final double price, @PathVariable final TradeAction action, @PathVariable String portfolioId){
        
        // create a trade object
        Trade trade = service.addTrade(ticker, quantity, price, action);
        System.out.println("made it");

        final String uri = "http://docker13.conygre.com:8090/portfolio/" + portfolioId; 
        final RestTemplate restTemplate = new RestTemplate();
        Trade result = restTemplate.postForObject(uri, trade, Trade.class);

        return result;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/{id}")
    public Optional<Trade> getById(@PathVariable("id") final ObjectId id){
        return service.getById(id);
    }

    
    // use this update primarily for cancelled or error cases
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method=RequestMethod.PUT, value="/{id}/{status}/{portfolioId}")
    public Trade updateTrade(@PathVariable("id") final ObjectId id, @PathVariable("status") final TradeStatus tradeStatus, @PathVariable String portfolioId) {

        // update trade in trade db
        Trade trade = service.updateTrade(id, tradeStatus);

        final String uri = "http://docker13.conygre.com:8090/portfolio/" + portfolioId;
        final RestTemplate restTemplate = new RestTemplate();
        Trade result = restTemplate.postForObject(uri, trade, Trade.class);

        return result;
    }


    // use this function to update from pending to filled/rejected/partially filled and from created to pending in the portfolio
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method=RequestMethod.PUT, value="/update/{portfolioId}")
    public Trade updateTrade(Trade trade) {

        final String uri = "http://docker13.conygre.com:8090/portfolio/1";
        final RestTemplate restTemplate = new RestTemplate();
        Trade result = restTemplate.postForObject(uri, trade, Trade.class);
        System.out.println("updated****************************");

        return result;
    }


    @RequestMapping(method=RequestMethod.GET, value = "/history/{ticker}")
    public Collection<Trade> getbyTicker(@PathVariable final String ticker){
        return service.getAllbyTicker(ticker);
    }

    @RequestMapping(method={RequestMethod.DELETE,RequestMethod.POST}, value = "/delete/{trade}")
    public String deleteTradeByTrade(@PathVariable final String trade){
        ObjectId tradeId = new ObjectId(trade);
        service.deleteTrade(tradeId);
        return "Trade deleted";
    }
}