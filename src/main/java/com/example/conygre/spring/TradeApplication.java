package com.example.conygre.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication()
@EnableMongoRepositories(basePackages = "com.example.conygre.spring.repo")
@EnableScheduling
public class TradeApplication {
    
    public static void main(String[] args) {
        System.out.println("Mandy's Branch");
        SpringApplication.run(TradeApplication.class, args);
    }
}